.. index::
   pair: webassembly ; Javascript interface
   ! JS-API

.. _wasm_javascript_interface:

=============================================
Webassembly Javascript Interface (JS-API)
=============================================

.. seealso::

   - https://webassembly.github.io/spec/js-api/index.html
   - https://github.com/WebAssembly/spec/labels/wasm-js-api-1
   - https://webassembly.org/getting-started/js-api/

.. contents::
   :depth: 3

Abstract
===========

This document provides an explicit JavaScript API for interacting with
WebAssembly.

This is part of a collection of related documents:

- the Core WebAssembly Specification,
- the WebAssembly JS Interface,
- and the WebAssembly Web API.
