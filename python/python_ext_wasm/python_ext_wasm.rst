
.. index::
   pair: webassembly ; python_ext_wasm

.. _python_ext_wasm:

==============================================================
python-ext-wasm (Python library to run WebAssembly binaries)
==============================================================

- https://github.com/wasmerio/python-ext-wasm

.. figure:: logo_python_wasm.png
   :align: center
   :width: 300

.. figure:: logo_wasmer.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
