
.. index::
   pair: wapm ; wasm
   pair: WebAssembly ; Package Manager
   ! wapm
   !  WebAssembly Package Manager

.. _wapm:

=======================================
WAPM (WebAssembly Package Manager)
=======================================

.. seealso::

   - https://github.com/wasmerio
   - https://x.com/wasmerio
   - https://github.com/wapmio
   - https://github.com/wapm-packages
   - https://medium.com/wasmer/announcing-wapm-the-webassembly-package-manager-18d52fae0eea
   - https://wapm.io/help/guide
   - https://wapm.io/help/reference
   - https://github.com/wapm-packages/rust-wasi-example


.. figure:: icon_wapm.png
   :align: center


.. toctree::
   :maxdepth: 3

   announce/announce
   installation/installation
   cli/cli
   help/help
