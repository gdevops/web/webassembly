
.. index::
   pair: wapm ; announce (2019-04-23)

.. _wapm_announce:

=============================
wapm announce (2019-04-23)
=============================

.. seealso::

   - https://medium.com/wasmer/announcing-wapm-the-webassembly-package-manager-18d52fae0eea
   - https://wapm.io/help/guide


.. contents::
   :depth: 3

Introduction
==============

Today, we are releasing a new tool that will help you use WebAssembly
anywhere: WAPM (aka WebAssembly Package Manager).

This release includes:

- A command line application: wapm, included when you install Wasmer
- A website and package registry: wapm.io


Why ?
======

While working on Wasmer, we discovered that the developer ergonomics needed to
improve significantly for WebAssembly to be accessible by the general audience.

We realized that a Package Manager will help solve common problems like
publishing, defining module ABIs, distributing executable binaries and
using them.

What ?
=========

WebAssembly is an abstraction on top of chipset instructions, this enables
wasm modules to run very easily on any machine.

If we move this abstraction up we can unlock the potential of having universal
binaries that can run anywhere, even on platforms/chipsets not supported at
the moment of releasing the binary.

Integrations like WASI and Emscripten are essential for projects that want to
target WebAssembly easily.

We aim to offer a Package Manager to be a companion to WebAssembly, that will
ease the use and distribution of WebAssembly modules.

Why do we need a Package Manager ?
====================================

Let’s dig a bit more into why a Package Manager is key for the adoption of
WebAssembly on the server-side.

Persona I: common user
------------------------

First, we have the use case of a developer that wants to use a WebAssembly
binary but has no idea how/where to find it: this can be nginx, lua, sqlite
or any other wasm binary. For them, this is how the process looked like:

Let’s do some search and see where can I find a runnable lua.wasmer

::

    wget https://github.com/wasmerio/wasmer/raw/master/examples/lua.wasm

Try to run it… what are the commands I can run on this binary ?::

    wasmer run lua.wasm -- -h


Persona II: universal binary publisher
------------------------------------------

Our second persona is a developer that wants to create a universal binary and
distribute it very easily across any machine and operating system.

This can be useful for command-line tools, or anything that can be run
directly in your shell.

Generate the WebAsseembly file (with tools like emcc)::

    emcc main.c -s WASM=1 # this emits the main.wasm file

Where should I publish it? Github? Npm? But it’s only the WebAssembly, no JS
associated… let’s do Github!

::

    git push ...


Persona III: universal library publisher
------------------------------------------

We also have the use case of a developer that wants to create universal
libraries in WebAssembly and use them from all languages (this can be: Python,
PHP, JavaScript, Rust, C, C++…).

This would be useful, for example, for developers that are creating libraries
that could be used across any language (like a lightweight embeddable search
engine, a face detection library, an universal GraphQL framework …).

Fortunately, we already have WebAssembly integrations for many of them !
Python, PHP, Rust, ...


All these use cases pushed us to re-think the current panorama of Package
Managers and how we can create one solely focused on WebAssembly that will
unite the following principles:

- It should make it easy to publish, download and use WebAssembly modules
- It should enable an easy way to define commands on top of wasm
- It should allow different ABIs: WASI, Emscripten, or even new ones in the future.
- Read a in-depth analysis of what’s a WebAssembly ABI here.
- It should be pluggable into any language ecosystem (Python, PHP, Ruby,
  JavaScript, ...), without forcing one ecosystem into other.

NPM
=====

Node.js Package Manager is great and has the most experience in hosting
WebAssembly modules (since WebAssembly was seeded in the JavaScript ecosystem).

However we felt that WebAssembly on the server-side is a completely new use case,
and as so it deserved a Package Manager shaped from the essence of WebAssembly,
and emancipated from the JavaScript ecosystem.

.. note:: while we think using WebAssembly from JavaScript it’s a great use
  case, we also want to enable and encourage WebAssembly use from Python, PHP,
  Rust, ...


WAPM 🎉
============

We have good news! WAPM is already included when you install wasmer::

    curl https://get.wasmer.io -sSfL | sh

You can start using it now. Let’s install cowsay and lolcat to see all the
awesomeness of it !

::

    wapm install cowsay lolcat


To make easy to adopt wapm, we have already published a lot of awesome packages,
so you can start using them !

- cowsay (running using Rust and WASI!): cows say things
- lolcat (WASI as well!): rainbows and unicorns!
- sqlite: database
- php: web programming language
- nginx: http server
- lua: programming language
- wabt: WebAssembly binary toolkit (we can run wasm2wat using the wabt binary
  tool compiled to wasm 🤯)

Publish your own WebAssembly modules
======================================

We are **very, very excited of what you will build with this**.

Register now in WAPM and start publishing your own packages, so anyone can
start using them anywhere !

We have created a guide to ease the initiation into wapm:  https://wapm.io/help/guide

We also created an example package to showcase how to create and publish a
package to wapm using Rust and the WASI target:


Welcome to the WebAssembly journey !
=======================================

Interested in working with us ?
Reach out to syrus@wasmer.io !
