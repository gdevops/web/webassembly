
.. index::
   pair: WebAssembly ; Installation

.. _wasm_installation:

=======================================
wasmer installation
=======================================

.. seealso::

   - https://github.com/wasmerio

::

    curl https://get.wasmer.io -sSfL | sh


.. figure:: install_wasmer.png
   :align: center

   https://github.com/wasmerio
