

.. _python_ext_wasm_def:

==============================================================
python_ext_wasm definition
==============================================================

- https://github.com/wasmerio/python-ext-wasm
- https://github.com/wasmerio/python-ext-wasm/blob/master/README.md

.. contents::
   :depth: 3


README.md
===========

- https://github.com/wasmerio/python-ext-wasm/blob/master/README.md
