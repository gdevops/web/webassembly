.. index::
   pair: webassembly ; Language
   ! WASM
   ! WebAssembly

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/webassembly/rss.xml>`_

.. _webassembly_language:
.. _wasm_tuto:

===============================================
|wasm| **Webassembly language** (WASM)
===============================================

- https://github.com/webassembly/spec/issues/
- https://github.com/WebAssembly/spec/graphs/contributors
- https://github.com/WebAssembly/spec/commits/master
- https://github.com/WebAssembly/spec/tree/master/interpreter
- https://people.mpi-sws.org/~rossberg/software.html#wasm
- https://www.w3.org/wasm/
- https://webassembly.org/
- https://www.w3.org/community/webassembly/
- https://lists.w3.org/Archives/Public/public-webassembly/
- https://webassembly.github.io/spec/js-api/index.html
- https://wasmer.io/
- https://wasmweekly.news/
- https://webassembly.github.io/spec/core/intro/introduction.html
- https://webassembly.org/getting-started/developers-guide/
- https://webassembly.org/docs/faq/
- https://www.w3.org/2017/08/wasm-charter
- https://github.com/WebAssembly/spec/tree/wg-1.0
- https://en.wikipedia.org/wiki/WebAssembly
- https://github.com/wasmerio/awesome-wasi
- https://x.com/WasmWeekly
- https://people.mpi-sws.org/~rossberg/
- https://webassembly.org/getting-started/js-api/


.. toctree::
   :maxdepth: 3

   description/description
   javascript_interface/javascript_interface
   news/news
   docs/docs
   packaging/packaging
   python/python
   tutorials/tutorials
   wasmer/wasmer
   versions/versions
