
.. index::
   pair: webassembly ; Python

.. _webassembly_python:

=============================
Webassembly python
=============================

.. toctree::
   :maxdepth: 3

   python_ext_wasm/python_ext_wasm
