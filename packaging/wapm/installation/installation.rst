
.. index::
   pair: wapm ; installation

.. _wapm_installation:

=============================
wapm installation
=============================

.. contents::
   :depth: 3


WAPM 🎉
========

.. seealso::

   - https://medium.com/wasmer/announcing-wapm-the-webassembly-package-manager-18d52fae0eea


We have good news! WAPM is already included when you install wasmer:

::

    curl https://get.wasmer.io -sSfL | sh


::

    wapm install cowsay lolcat


.. figure:: cowsay_lolcat.png
   :align: center
