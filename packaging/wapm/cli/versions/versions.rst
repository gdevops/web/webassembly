
.. index::
   pair: wapm CLI; Versions

.. _wapm_cli_versions:

=======================================
WAPM CLI versions
=======================================

.. seealso::

   - https://github.com/wasmerio/wapm-cli/releases

.. toctree::
   :maxdepth: 3

   0.5.0/0.5.0
   0.1.0/0.1.0
