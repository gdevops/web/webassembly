

.. index::
   pair: Webassembly; docs

.. _webassembly_docs:

=============================
Webassembly docs
=============================

.. toctree::
   :maxdepth: 3

   faq/faq
   non_web/non_web
