.. index::
   pair: webassembly ; Versions

.. _webassembly_versions:

=============================
Webassembly versions
=============================

.. seealso::

   - https://github.com/WebAssembly/spec/releases


.. toctree::
   :maxdepth: 3

   wasmnext/wasmnext
   1.0.0/1.0.0
