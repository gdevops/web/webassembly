.. index::
   pair: webassembly ; 1.0.0 (2019-07-19)

.. _webassembly_1_0_0:

===========================================
Webassembly spec **1.0.0 (2019-07-19)**
===========================================

.. seealso::

   - https://github.com/WebAssembly/spec/tree/wg-1.0


.. figure:: browsers.png
   :align: center

   https://webassembly.org/
