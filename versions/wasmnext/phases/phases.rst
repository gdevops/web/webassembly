.. index::
   pair: webassembly ; phases

.. _webassembly_phases:

==================================
**Webassembly process phases**
==================================

.. seealso::

   - https://github.com/WebAssembly/meetings/blob/master/process/phases.md
