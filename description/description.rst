
.. _webassembly_description:

=============================
Webassembly description
=============================

.. contents::
   :depth: 3


Andreas Rossberg definition
==============================

.. seealso::

   - https://people.mpi-sws.org/~rossberg/software.html#wasm

WebAssembly is a safe and portable format for efficient native-code
execution, available in browsers and other embeddings.

**I am the author of the language specification** and maintainer of the
reference interpreter, as well as owner of various feature proposals
in the works.

:Homepage: https://webassembly.github.io/
:Specification: https://webassembly.github.io/spec/core/
:Reference Interpreter: https://github.com/WebAssembly/spec/tree/master/interpreter


https://webassembly.org/docs/use-cases/
========================================

.. seealso::

   - https://webassembly.org/docs/use-cases/

Use Cases
------------

WebAssembly’s high-level goals define what WebAssembly aims to achieve,
and in which order.

How WebAssembly achieves its goals is documented for Web and non-Web
platforms.

The following is an unordered and incomplete list of applications/domains/computations
that would benefit from WebAssembly and are being considered as use
cases during the design of WebAssembly.

Inside the browser
--------------------

- Better execution for languages and toolkits that are currently
  cross-compiled to the Web (C/C++, GWT, …).
- Image / video editing.
- Games:

    - Casual games that need to start quickly.
    - AAA games that have heavy assets.
    - Game portals (mixed-party/origin content).

- Peer-to-peer applications (games, collaborative editing, decentralized
  and centralized).
- Music applications (streaming, caching).
- Image recognition.
- Live video augmentation (e.g. putting hats on people’s heads).
- VR and augmented reality (very low latency).
- CAD applications.
- Scientific visualization and simulation.
- Interactive educational software, and news articles.
- Platform simulation / emulation (ARC, DOSBox, QEMU, MAME, …).
- Language interpreters and virtual machines.
- POSIX user-space environment, allowing porting of existing POSIX applications.
- Developer tooling (editors, compilers, debuggers, …).
- Remote desktop.
- VPN.
- Encryption.
- Local web server.
- Common NPAPI users, within the web’s security model and APIs.
- Fat client for enterprise applications (e.g. databases).

Outside the browser
----------------------

- Game distribution service (portable and secure).
- Server-side compute of untrusted code.
- Server-side application.
- Hybrid native apps on mobile devices.
- Symmetric computations across multiple nodes

How WebAssembly can be used
--------------------------------

- Entire code base in WebAssembly.
- Main frame in WebAssembly, but the UI is in JavaScript / HTML.
- Re-use existing code by targeting WebAssembly, embedded in a larger
  JavaScript / HTML application.

  This could be anything from simple helper libraries, to compute-oriented
  task offload.




Wikipedia definition
======================

.. seealso::

   - https://en.wikipedia.org/wiki/WebAssembly

WebAssembly (often shortened to Wasm) is a standard that defines a binary
format and a corresponding assembly-like text format for executables used
by web pages.

The purpose of Wasm is to enable the JavaScript engine of a web browser to
execute page scripts nearly as fast as native machine code.

But this is not a full replacement for JavaScript; rather, Wasm is only
intended for performance-critical portions of page scripts.

Wasm code runs in the same sandbox as regular script code, but only regular
scripts have direct access to the DOM tree.

The World Wide Web Consortium (W3C) maintains the standard with contributions
from Mozilla, Microsoft, Google, and Apple.

WebAssembly definition
========================

.. seealso::

   - https://webassembly.org/

WebAssembly (abbreviated Wasm) is a binary instruction format for a stack-based
virtual machine.

Wasm is designed as a portable target for compilation of high-level languages
like C/C++/Rust, enabling deployment on the web for client and server applications.


Efficient and fast
--------------------

The Wasm stack machine is designed to be encoded in a size- and load-time-efficient
binary format.

WebAssembly aims to execute at native speed by taking advantage of common
hardware capabilities available on a wide range of platforms.


Mozilla definition
====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/WebAssembly


Definition 1
---------------

WebAssembly is a new type of code that can be run in modern web browsers
it is a low-level assembly-like language with a compact binary format
that runs with near-native performance and provides languages such as
C/C++ and Rust with a compilation target so that they can run on the web.

**It is also designed to run alongside JavaScript**, allowing both to work together.


In a Nutshell (2020-05-23)
------------------------------

WebAssembly has huge implications for the web platform — it provides a
way to run code written in multiple languages on the web at near native
speed, with client apps running on the web that previously couldn’t
have done so.

WebAssembly is designed to complement and run alongside JavaScript
using the **WebAssembly JavaScript APIs**, you can load WebAssembly
modules into a JavaScript app and share functionality between the two.

This allows you to take advantage of WebAssembly's performance and
power and JavaScript's expressiveness and flexibility in the same apps,
even if you don't know how to write WebAssembly code.

And what's even better is that it is being developed as a web standard
via the W3C WebAssembly Working Group and Community Group with active
participation from all major browser vendors.
