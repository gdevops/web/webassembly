
.. index::
   pair: wasmer ; Versions

.. _wasmer_versions:

=======================================
wasmer versions
=======================================

.. seealso::

   - https://github.com/wasmerio/wasmer/releases

.. toctree::
   :maxdepth: 3

   0.4.0/0.4.0
