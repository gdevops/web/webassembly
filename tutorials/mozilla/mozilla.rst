
.. index::
   pair: webassembly ; Mozilla

.. _webassembly_mozilla:

=============================
Webassembly mozilla tutorial
=============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/WebAssembly
