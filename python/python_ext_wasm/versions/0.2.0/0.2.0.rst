
.. index::
   pair: 0.2.0 (2019-04-16) ; python_ext_wasm

.. _python_ext_wasm_0_2_0:

====================================
python_ext_wasm 0.2.0 (2019-04-16)
====================================

.. seealso::

   - https://github.com/wasmerio/python-ext-wasm/tree/0.2.0
