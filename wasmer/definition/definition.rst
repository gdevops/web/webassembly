
.. index::
   pair: wasmer ; definition

.. _wasmer_def:

=======================================
wasmer definition
=======================================

.. seealso::

   - https://github.com/wasmerio/wasmer

.. contents::
   :depth: 3

Introduction
------------

`Wasmer <https://wasmer.io/>`__ is a standalone JIT WebAssembly runtime,
aiming to be fully compatible with
`WASI <https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/>`__
and `Emscripten <https://emscripten.org/>`__.

Install Wasmer with:

.. code:: sh

    curl https://get.wasmer.io -sSfL | sh

Wasmer runtime can also be embedded in different languages, so you can
use WebAssembly anywhere ✨: \*:

`**Rust** <https://github.com/wasmerio/wasmer-rust-example>`__ \*
`**C/C++** <https://github.com/wasmerio/wasmer-c-api>`__ \*
`**PHP** <https://github.com/wasmerio/php-ext-wasm>`__ \*
`**Python** <https://github.com/wasmerio/python-ext-wasm>`__

Usage
~~~~~

Wasmer can execute both the standard binary format (``.wasm``) and the
text format defined by the WebAssembly reference interpreter (``.wat``).

Once installed, you will be able to run any WebAssembly files
(*including Lua, PHP, SQLite and nginx!*):

.. code:: sh

    # Run Lua
    wasmer run examples/lua.wasm

    # Run PHP
    wasmer run examples/php.wasm

    # Run SQLite
    wasmer run examples/sqlite.wasm

    # Run nginx
    wasmer run examples/nginx/nginx.wasm -- -p examples/nginx -c nginx.conf

Code Structure
--------------

Wasmer is structured into different directories:

-  ```src`` <./src>`__: code related to the Wasmer executable itself
-  ```lib`` <./lib>`__: modularized libraries that Wasmer uses under the
   hood
-  ```examples`` <./examples>`__: some useful examples to getting
   started with Wasmer

Dependencies
------------

Building Wasmer requires `rustup <https://rustup.rs/>`__.

To build on Windows, download and run
```rustup-init.exe`` <https://win.rustup.rs/>`__ then follow the
onscreen instructions.

To build on other systems, run:

.. code:: sh

    curl https://sh.rustup.rs -sSf | sh

Other dependencies
~~~~~~~~~~~~~~~~~~

Please select your operating system:

-  `macOS <#macos>`__
-  `Debian-based Linuxes <#debian-based-linuxes>`__
-  `FreeBSD <#freebsd>`__
-  `Microsoft Windows <#windows-msvc>`__

macOS
^^^^^

If you have `Homebrew <https://brew.sh/>`__ installed:

.. code:: sh

    brew install cmake

Or, in case you have
`MacPorts <https://www.macports.org/install.php>`__:

.. code:: sh

    sudo port install cmake

Debian-based Linuxes
^^^^^^^^^^^^^^^^^^^^

.. code:: sh

    sudo apt install cmake pkg-config libssl-dev

FreeBSD
^^^^^^^

.. code:: sh

    pkg install cmake

Windows (MSVC)
^^^^^^^^^^^^^^

Windows support is *highly experimental*. Only simple Wasm programs may
be run, and no syscalls are allowed. This means nginx and Lua do not
work on Windows. See `this
issue <https://github.com/wasmerio/wasmer/issues/176>`__ regarding
Emscripten syscall polyfills for Windows.

1. Install `Visual
   Studio <https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15>`__

2. Install `Rust for Windows <https://win.rustup.rs>`__

3. Install `Python for
   Windows <https://www.python.org/downloads/release/python-2714/>`__.
   The Windows x86-64 MSI installer is fine. Make sure to enable "Add
   python.exe to Path" during installation.

4. Install `Git for Windows <https://git-scm.com/download/win>`__. Allow
   it to add ``git.exe`` to your PATH (default settings for the
   installer are fine).

5. Install `CMake <https://cmake.org/download/>`__. Ensure CMake is in
   your PATH.

6. Install `LLVM
   7.0 <https://prereleases.llvm.org/win-snapshots/LLVM-7.0.0-r336178-win64.exe>`__

Building
--------

Wasmer is built with `Cargo <https://crates.io/>`__, the Rust package
manager.

.. code:: sh

    # checkout code
    git clone https://github.com/wasmerio/wasmer.git
    cd wasmer

    # install tools
    # make sure that `python` is accessible.
    cargo install --path .

Testing
-------

Thanks to `spec
tests <https://github.com/wasmerio/wasmer/tree/master/lib/spectests/spectests>`__
we can ensure 100% compatibility with the WebAssembly spec test suite.

Tests can be run with:

.. code:: sh

    make test

If you need to regenerate the Rust tests from the spec tests you can
run:

.. code:: sh

    make spectests

You can also run integration tests with:

.. code:: sh

    make integration-tests

Benchmarking
------------

Benchmarks can be run with:

.. code:: sh

    cargo bench --all

Roadmap
-------

Wasmer is an open project guided by strong principles, aiming to be
modular, flexible and fast. It is open to the community to help set its
direction.

Below are some of the goals of this project (in order of priority):

-  [x] It should be 100% compatible with the `WebAssembly spec
   tests <https://github.com/wasmerio/wasmer/tree/master/lib/spectests/spectests>`__
-  [x] It should be fast *(partially achieved)*
-  [x] Support WASI - released in
   `0.3.0 <https://github.com/wasmerio/wasmer/releases/tag/0.3.0>`__
-  [ ] Support Emscripten calls *(in the works)*
-  [ ] Support Rust ABI calls
-  [ ] Support Go ABI calls

Architecture
------------

If you would like to know how Wasmer works under the hood, please see
`ARCHITECTURE.md <./ARCHITECTURE.md>`__.

License
-------

Wasmer is primarily distributed under the terms of the `MIT
license <http://opensource.org/licenses/MIT>`__
(`LICENSE <./LICENSE>`__).

`ATTRIBUTIONS <./ATTRIBUTIONS.md>`__
