
.. index::
   pair: wapm ; help

.. _wapm_help:

=======================================
WAPM help
=======================================

.. seealso::

   - https://wapm.io/help/guide
   - https://wapm.io/help/reference
   - https://github.com/wapm-packages/rust-wasi-example
