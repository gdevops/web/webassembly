
.. index::
   pair: Versions ; python_ext_wasm

.. _python_ext_wasm_versions:

=============================
python_ext_wasm versions
=============================

.. seealso::

   - https://github.com/wasmerio/python-ext-wasm/releases


.. toctree::
   :maxdepth: 3


   0.4.1/0.4.1
   0.2.0/0.2.0
