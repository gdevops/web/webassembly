
.. index::
   pair: wapm ; CLI

.. _wapm_cli_def:

=======================================
WAPM CLI definition
=======================================

.. seealso::

   - https://github.com/wasmerio/wapm-cli/blob/master/README.md

.. contents::
   :depth: 3

Introduction
============

The WebAssembly Package Manager CLI. This tool enables installing,
managing, and publishing wasm packages on the `wapm.io <https://wapm.io>`__ registry.

Get Started
-----------

Read the ```wapm-cli`` user guide on
``wapm.io`` <https://wapm.io/help/guide>`__ to get started using the
tool and use the ```wapm-cli``
reference <https://wapm.io/help/reference>`__ for information about the
cli commands.

Get Help
--------

Join the discussion on `spectrum chat <https://spectrum.chat/wasmer>`__
in the ``wapm-cli`` channel, or create a GitHub issue. We love to help!

Contributing
------------

See the `contributing guide <CONTRIBUTING.md>`__ for instruction on
contributing to ``wapm-cli``.

Development
-----------

Update GraphQL Schema
~~~~~~~~~~~~~~~~~~~~~

If the WAPM GraphQL server has been updated, update the GraphQL schema
with:

::

    graphql get-schema -e prod

*Note: You will need graphql-cli installed for it
``npm install -g graphql-cli``.*
