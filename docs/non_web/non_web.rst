

.. index::
   pair: Webassembly; non-web

.. _webassembly_non_web:

=============================
Webassembly no-web
=============================

.. seealso::

   - https://webassembly.org/docs/non-web/

.. contents::
   :depth: 3

Non-Web Embeddings
===================

While WebAssembly is designed to run on the Web, it is also desirable for it
to be able to execute well in other environments, including everything from
minimal shells for testing to full-blown application environments e.g. on
servers in datacenters, on IoT devices, or mobile/desktop apps.

It may even be desirable to execute WebAssembly embedded within larger programs.

Non-Web environments may provide different APIs than Web environments, which
feature testing and dynamic linking will make discoverable and usable.

Non-Web environments may include JavaScript VMs (e.g. node.js), however
WebAssembly is also being designed to be capable of being executed without
a JavaScript VM present.
