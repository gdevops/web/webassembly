.. index::
   pair: webassembly ; proposals

.. _webassembly_proposals:

==================================
**Webassembly proposals**
==================================

.. seealso::

   - https://github.com/WebAssembly/proposals
   - https://github.com/WebAssembly/proposals/commits/master
   - https://github.com/WebAssembly/proposals/graphs/contributors


.. toctree::
   :maxdepth: 3

   phases/phases
