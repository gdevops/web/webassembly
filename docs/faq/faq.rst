

.. index::
   pair: Webassembly; FAQ

.. _webassembly_faq:

=============================
Webassembly FAQ
=============================

.. seealso::

   - https://webassembly.org/docs/faq/

.. contents::
   :depth: 3

Is WebAssembly trying to replace JavaScript ?
===============================================

No! WebAssembly is designed to be a complement to, not replacement of,
JavaScript.

While WebAssembly will, over time, allow many languages to be compiled to
the Web, JavaScript has an incredible amount of momentum and will remain the
single, privileged (as described above) dynamic language of the Web.

Furthermore, it is expected that JavaScript and WebAssembly will be used
together in a number of configurations:

- Whole, compiled C++ apps that leverage JavaScript to glue things together.
- HTML/CSS/JavaScript UI around a main WebAssembly-controlled center canvas,
  allowing developers to leverage the power of web frameworks to build
  accessible, web-native-feeling experiences.
- Mostly HTML/CSS/JavaScript app with a few high-performance WebAssembly
  modules (e.g., graphing, simulation, image/sound/video processing,
  visualization, animation, compression, etc., examples which we can already
  see in asm.js today) allowing developers to reuse popular WebAssembly
  libraries just like JavaScript libraries today.
- When WebAssembly gains the ability to access garbage-collected objects :unicorn:,
  those objects will be shared with JavaScript, and not live in a walled-off
  world of their own.
