.. index::
   ! wasmer

.. _wasmer:

=========================================================================================
**wasmer** (The **Universal WebAssembly Runtime** supporting WASI and Emscripten)
=========================================================================================

.. seealso::

   - https://github.com/wasmerio/wasmer
   - https://github.com/wasmerio
   - https://x.com/wasmerio
   - https://wasmer.io/
   - https://spectrum.chat/wasmer?tab=posts


.. figure:: wasmer-logo.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   versions/versions
