
.. index::
   pair: wapm ; CLI

.. _wapm_cli:

=======================================
WAPM CLI
=======================================

.. seealso::

   - https://github.com/wasmerio/wapm-cli
   - https://wapm.io/help/guide

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
